package database

import (
	"context"

	pgPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/db/postgres"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/status-list-service/internal/entity"
)

type DbConnection interface {
	AllocateIndexInCurrentList(ctx context.Context, tenantId string) (*entity.StatusData, error)
	RevokeCredentialInSpecifiedList(ctx context.Context, tenantId string, listId int, index int) error
	CreateTableForTenantIdIfNotExists(ctx context.Context, tenantId string) error
	GetStatusList(ctx context.Context, tenantId string, listId int) ([]byte, error)
	CacheList(ctx context.Context, cacheId string, list []byte) error
	Ping() bool
	Close()
}

// TablePrefix is needed for table name cause of postgres name convention -> no integers allowed
const TablePrefix = "tenant_id_"

type Database struct {
	DbConnection
}

func New(ctx context.Context, config pgPkg.Config, listSizeInBytes int) (*Database, error) {
	dbConnection, err := newPostgresConnection(config, ctx, listSizeInBytes)
	return &Database{DbConnection: dbConnection}, err
}
