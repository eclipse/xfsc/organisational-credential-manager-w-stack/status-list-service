package api

import (
	"sync"

	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/status-list-service/internal/config"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/status-list-service/internal/database"
)

var db *database.Database

func Listen(database *database.Database, conf *config.StatusListConfiguration) {
	var wg sync.WaitGroup

	db = database

	wg.Add(2)
	go startMessaging(conf, &wg)

	go startRest(conf, &wg, db)

	wg.Wait()
}
