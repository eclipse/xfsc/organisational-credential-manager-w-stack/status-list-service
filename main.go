package main

import (
	"context"

	log "github.com/sirupsen/logrus"
	ctxPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/ctx"
	logPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/status-list-service/internal/api"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/status-list-service/internal/config"
	"gitlab.eclipse.org/eclipse/xfsc/organisational-credential-manager-w-stack/status-list-service/internal/database"
)

func main() {
	ctx := context.Background()

	if err := config.Load(); err != nil {
		log.Fatalf("failed to load config: %v", err)
	}

	currentConf := &config.CurrentStatusListConfig

	logger, err := logPkg.New(currentConf.LogLevel, currentConf.IsDev, nil)
	if err != nil {
		log.Fatalf("failed to init logger: %v", err)
	}

	ctx = ctxPkg.WithLogger(ctx, *logger)
	config.SetLogger(*logger)
	dbConf := &currentConf.Database

	db, err := database.New(ctx, *dbConf, currentConf.ListSizeInBytes)

	if err != nil {
		log.Fatalf("database cant be established: %v", err)
	}

	api.Listen(db, currentConf)
}
